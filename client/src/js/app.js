import React, { Component } from "react";
import { render } from "react-dom";
import AddTask from "../components/AddTask";
import Task from "../components/Task";
import "../css/style.css";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      tasks: []
    };
  }
  getTodos() {
    fetch("/todos/")
      .then(response => response.json())
      .then(data => this.setState({ tasks: data }));
  }
  componentWillMount() {
    this.getTodos();
  }

  componentDidMount() {
    this.getTodos();
  }

  handleAddTAsk = task => {
    let newTask = {
      _id: task.id,
      task: task.task,
      chk: task.chk
    };
    return fetch("/todos/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify(newTask)
    })
      .then(response => response.json())
      .then(this.componentDidMount())
      .catch(error => console.log(error.message));
  };

  handleDeleteTask = id => {
    return fetch("/todos/" + id, {
      method: "DELETE"
    })
      .then(this.componentDidMount())
      .catch(error => console.log(error.message));
  };

  handleCheckTask = (id, checkVal) => {
    let newTask = {
      chk: checkVal
    };
    return fetch("/todos/" + id, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify(newTask)
    })
      .then(response => response.json())
      .then(this.componentDidMount())
      .catch(error => console.log(error.message));
  };

  render() {
    return (
      <div className="app">
        <AddTask addTask={this.handleAddTAsk} />
        <Task
          tasks={this.state.tasks}
          onChecked={this.handleCheckTask}
          onDelete={this.handleDeleteTask}
        />
      </div>
    );
  }
}

render(<App />, document.getElementById("app"));
