const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TodoSchema = new Schema({
  _id: {
    type: String,
    required: [true, "id is required"]
  },
  task: {
    type: String,
    required: [true, "task is required"]
  },
  chk: {
    type: Boolean,
    required: [true, "check is required"]
  }
});

const TodoList = mongoose.model("todoList", TodoSchema);

module.exports = TodoList;
