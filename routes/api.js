const express = require("express");
const router = express.Router();
const TodoList = require("../models/todoList.js");

router.get("/", function(req, res) {
  res.render("index");
});
//getting list from database
router.get("/todos", function(req, res, next) {
  TodoList.find({}).then(function(todos) {
    res.send(todos);
  });
});

//adding to database
router.post("/todos", function(req, res, next) {
  TodoList.create(req.body)
    .then(function(todos) {
      res.send(todos);
    })
    .catch(next);
});

//update a list
router.put("/todos/:id", function(req, res, next) {
  TodoList.findByIdAndUpdate({ _id: req.params.id }, req.body).then(function() {
    TodoList.findOne({ _id: req.params.id }).then(function(todos) {
      res.send(todos);
    });
  });
});

//delete a list
router.delete("/todos/:id", function(req, res, next) {
  TodoList.findByIdAndRemove({ _id: req.params.id }).then(function(todos) {
    res.send(todos);
  });
});

module.exports = router;
